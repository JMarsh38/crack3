#include <stdlib.h>
#include <stdio.h>
#include "bintree.h"
#include <string.h>

node insert(char* word, char* hash, node **leaf)
{
    char *key = hash;
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        strcpy((*leaf)->password,word);
        //(*leaf)->password = word;
        //(*leaf)->hash = key;
        strcpy((*leaf)->hash,key);
        
        // initialize the children to null 
        (*leaf)->left = NULL; 
        (*leaf)->right = NULL; 
        //printf("%s\n",(*leaf)->password);     //prints correctly
    }
    else if (strcmp(key,(*leaf)->hash)<0)
        insert (word, key, &((*leaf)->left));
    else if (strcmp(key,(*leaf)->hash)>0)
        insert (word, key, &((*leaf)->right));
    return **leaf;
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s,     %s\n", leaf->hash, leaf->password);     //prints password incorrecly, hash fine
	if (leaf->right != NULL) print(leaf->right);
}

// TODO: Modify so the key is the hash to search for
node *search(char* key, node *leaf)
{
  if (leaf != NULL)
  {
      if (strcmp(key,leaf->hash)==0)
         return leaf;
      else if (strcmp(key,leaf->hash)<0)
         return search(key, leaf->left);
      else
         return search(key, leaf->right);
  }
  else return NULL;
}


//int main()
//{
    /*
    node *tree = NULL;
    
    insert(5, &tree);
    insert(2, &tree);
    insert(8, &tree);
    insert(10, &tree);
    insert(1, &tree);
    insert(4, &tree);
    insert(8, &tree);
    insert(7, &tree);

    print(tree);
    */
//}

