#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

#include "bintree.h"




const int PASS_LEN=70;        // Maximum any password can be
const int HASH_LEN=50;        // Length of MD5 hash strings

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.

char **read_hashes(char *filename)
{

    //create inital array of 50 pointers 
    int arrlen = 50;
    char ** arr = (char**) malloc(arrlen*sizeof(char*));
    //read in file, expanding array as you go
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s\n", filename);
        exit(1);
    }
    char line[40];
    int i=0;
    while(fscanf(f, "%s", line)!=EOF)
    {
        arr[i] = malloc((strlen(line)+1)*sizeof(char));
        strcpy(arr[i], line);
        i++;
        if (i == arrlen)
        {
            //make array bigger
            arrlen = (int)(arrlen * 1.25);
            char **newarr = realloc(arr,arrlen*sizeof(char *));      //realoc takes array and new size, recreates array to new size.  
            if(newarr != NULL) arr = newarr;
            else exit(3);
        }
    }
    arr[i] = NULL;
    fclose(f);
    return arr;
    
}

// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.

node *read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    char line[50];
    node *firstnode = NULL;
    while(fscanf(f, "%s", line)!=EOF)
    {
        insert(line,md5(line,strlen(line)),&firstnode);
    }
    fclose(f);
    return firstnode;    
}


int main(int argc, char *argv[])
{
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);
    ;
    // TODO: Read the dictionary file into a binary tree
    
    node *dict = read_dict(argv[2]);

    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int i=0;
    while (hashes[i]!=NULL)
    {
    node foundnode = *search(hashes[i],dict);
    printf("%s, %s\n", foundnode.password, foundnode.hash);
    i++;
    }
}
