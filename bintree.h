// TODO: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
	char password[50]; 
	char hash[50];
	struct node *left;
	struct node *right;
} node;

node insert(char* hash, char* word, node **leaf);

void print(node *leaf);

node *search(char* word, node *leaf);
